const path = require('path');
const express = require('express');
const multer = require('multer');
const {nanoid} = require('nanoid');
const config = require('../config');
const Item = require("../models/Item");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const criteria = {};

        if (req.query.category) {
            criteria.category = req.query.category;
        }

        const items = await Item.find(criteria).populate('category', 'title');
        res.send(items);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const item = await Item.findOne({_id: req.params.id}).populate('category', 'title');

        if (item) {
            res.send(item);
        } else {
            res.sendStatus(404);
        }
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', upload.single('image'), async (req, res) => {
    try {
        const itemData = req.body;

        if (req.file) {
            itemData.image = 'uploads/' + req.file.filename;
        }

        const item = new Item(itemData);
        await item.save();

        res.send(item);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;