import React from 'react';
import {Container, CssBaseline} from "@material-ui/core";
import {Route, Switch} from 'react-router-dom';
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import Items from "./containers/Items/Items";
import NewItem from "./containers/Items/newItem";
import ItemInfo from "./containers/Items/ItemInfo";
import CategoryInfo from "./containers/Category/CategoryInfo";


const App = () => (
    <>
      <CssBaseline/>
      <header>
        <AppToolbar/>
      </header>
      <main>
        <Container maxWidth="xl">
          <Switch>
            <Route path="/" exact component={Items} />
            <Route path="/register" component={Register} />
            <Route path="/login" component={Login} />
            <Route path="/new" component={NewItem} />
            <Route path="/items/:id" component={ItemInfo} />
            <Route path="/categories/:id" component={CategoryInfo} />
          </Switch>
        </Container>
      </main>
    </>
);

export default App;
