import React from 'react';
import {Grid, TextField} from "@material-ui/core";
import PropTypes from 'prop-types';



const FormElement = ({name, label, value, onChange, required, error, type, autoComplete}) => {
    return (
        <Grid item xs>
            <TextField
                variant="outlined"
                fullWidth
                required={required}
                error={!!error}
                helperText={error}
                id={name}
                label={label}
                type={type}
                name={name}
                value={value}
                onChange={onChange}
                autoComplete={autoComplete}
            />
        </Grid>
    );
};

FormElement.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired,
    required: PropTypes.bool,
    type: PropTypes.string,
    error: PropTypes.string
};
export default FormElement;