import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchCategories} from "../../store/actions/catagoriesActions";
import Grid from "@material-ui/core/Grid";
import {Link} from "react-router-dom";

const Category = () => {
    const dispatch = useDispatch();
    const categories = useSelector((state) => state.categories.categories)

    useEffect(() => {
        dispatch(fetchCategories());
    }, [dispatch]);

    console.log(categories);
    return (
        <>
            <Grid container direction="column" spacing={2}>
                <Grid item xs>
                    <Link to={'/'}>All items</Link>
                </Grid>
                {categories.map((category) => (
                    <Grid item key={category._id}>
                        <Link to={'/categories/' + category._id}>{category.title}</Link>
                    </Grid>
                ))}
            </Grid>
        </>
    );
};

export default Category;