import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchItemInfo} from "../../store/actions/itemsActions";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {CardMedia, Paper} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
    itemMessage: {
        border: "2px solid grey",
        margin: "10px",
        padding: "5px",
    },
    cover: {
        width: '250px',
        height: '150px',
        overflow: 'hidden',
        borderRadius: '4px'
    },
}));

const ItemInfo = (props) => {
    const dispatch = useDispatch();
    const item = useSelector(state => state.items.item);
    const user = useSelector((state) => state.users.user)
    const classes = useStyles();
    console.log(item);

    useEffect(() => {
        dispatch(fetchItemInfo(props.match.params.id))
    }, [dispatch, props.match.params.id]);

    return (
        <>
            <Grid container direction="column" spacing={2}>
                <Paper className={classes.itemMessage}>
                    <h4>{item.category}</h4>
                    {/*<b>Author: {item.author}</b>*/}
                    <p>{item.title}</p>
                    <CardMedia
                        className={classes.cover}
                        image={'http://localhost:8000/' + item.image}
                    />
                </Paper>
                {/*{user ? (*/}
                {/*    <Grid item>*/}
                {/*            <Button variant="outlined">*/}
                {/*                Del*/}
                {/*            </Button>*/}
                {/*    </Grid>*/}
                {/*) : null}*/}
            </Grid>
        </>

    );
};

export default ItemInfo;