import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {fetchItems} from "../../store/actions/itemsActions";
import {CircularProgress, makeStyles} from "@material-ui/core";
import Item from "./Item";
import Category from "../Category/Category";


const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));


const Items = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const items = useSelector((state => state.items.items));
    const loading = useSelector((state => state.items.itemsLoading));

    useEffect(() => {
        dispatch(fetchItems());
    }, [dispatch]);

    return (
        <>
            <Grid container direction="column" spacing={2}>
                <Grid item container justify="space-between" alignItems="center">
                    <Grid item>
                        <Typography variant="h5">All items for BAZARCHIK</Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={2} alignItems="flex-start">
                    <Grid item xs={1}>
                        <Category/>
                    </Grid>
                    <Grid item xs>
                        <Grid item container spacing={1}>
                            {loading ? (
                                <Grid container justify="center" alignItems="center" className={classes.progress}>
                                    <Grid item>
                                        <CircularProgress />
                                    </Grid>
                                </Grid>
                            ) : items.map(item => (
                                <Item
                                    key={item._id}
                                    id={item._id}
                                    title={item.title}
                                    price={item.price}
                                    image={item.image}
                                />
                            ))}
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </>
    );
};

export default Items;