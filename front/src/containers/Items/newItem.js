import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchCategories} from "../../store/actions/catagoriesActions";
import {createItem} from "../../store/actions/itemsActions";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import ItemForm from "../../components/ItemForm/ItemForm";

const NewItem = ({history}) => {
    const dispatch = useDispatch();
    const categories = useSelector(state => state.categories.categories);

    useEffect(() => {
        dispatch(fetchCategories());
    }, [dispatch]);

    const onItemFormSubmit = async itemData => {
        await dispatch(createItem(itemData));
        history.push('/');
    };
    return (
        <Grid container direction="column" spacing={2}>
            <Grid item xs>
                <Typography variant="h4">Add new Item</Typography>
            </Grid>
            <Grid item xs>
                <ItemForm onSubmit={onItemFormSubmit} categories={categories}/>
            </Grid>
        </Grid>
    );
};

export default NewItem;