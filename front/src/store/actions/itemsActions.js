import axiosApi from "../../axiosApi";
import {NotificationManager} from 'react-notifications';


export const FETCH_ITEMS_REQUEST = 'FETCH_ITEMS_REQUEST';
export const FETCH_ITEMS_SUCCESS = 'FETCH_ITEMS_SUCCESS';
export const FETCH_ITEMS_FAILURE = 'FETCH_ITEMS_FAILURE';

export const FETCH_ITEM_INFO_REQUEST = 'FETCH_ITEM_INFO_REQUEST';
export const FETCH_ITEM_INFO_SUCCESS = 'FETCH_ITEM_INFO_SUCCESS';
export const FETCH_ITEM_INFO_FAILURE = 'FETCH_ITEM_INFO_FAILURE';

export const CREATE_ITEM_SUCCESS = 'CREATE_ITEM_SUCCESS';

export const fetchItemsRequest = () => ({type: FETCH_ITEMS_REQUEST});
export const fetchItemsSuccess = items => ({type: FETCH_ITEMS_SUCCESS, items});
export const fetchItemsFailure = () => ({type: FETCH_ITEMS_FAILURE});

export const fetchItemInfoRequest = () => ({type: FETCH_ITEM_INFO_REQUEST});
export const fetchItemInfoSuccess = item => ({type: FETCH_ITEM_INFO_SUCCESS, item});
export const fetchItemInfoFailure = () => ({type: FETCH_ITEM_INFO_FAILURE});

export const createItemSuccess = () => ({type: CREATE_ITEM_SUCCESS});

export const fetchItems = () => {
    return async dispatch => {
        try {
            dispatch(fetchItemsRequest());
            const response = await axiosApi.get('/items');
            dispatch(fetchItemsSuccess(response.data));
        } catch (e) {
            dispatch(fetchItemsFailure());
            NotificationManager.error('Could not fetch products');
        }
    }
};

export const fetchItemInfo = (id) => {
    return async dispatch => {
        try {
            dispatch(fetchItemInfoRequest());
            const response = await axiosApi.get('/items/' + id);
            console.log(response);
            dispatch(fetchItemInfoSuccess(response.data));
        } catch (e) {
            dispatch(fetchItemInfoFailure(e));
            NotificationManager.error('Could not fetch products');
        }
    }
};

export const createItem = itemData => {
    return async dispatch => {
        await axiosApi.post('/items', itemData);
        dispatch(createItemSuccess());
    };
};
