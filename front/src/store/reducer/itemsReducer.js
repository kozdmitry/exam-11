import {
    FETCH_ITEM_INFO_FAILURE,
    FETCH_ITEM_INFO_REQUEST, FETCH_ITEM_INFO_SUCCESS,
    FETCH_ITEMS_FAILURE,
    FETCH_ITEMS_REQUEST,
    FETCH_ITEMS_SUCCESS
} from "../actions/itemsActions";

const initialState = {
    items: [],
    item: null,
    itemsLoading: false,
    itemLoading: false,
};

const itemsReducer = (state = initialState, action) => {
    switch (action.type){
        case FETCH_ITEMS_REQUEST:
            return {...state, itemsLoading: true};
        case FETCH_ITEMS_SUCCESS:
            return {...state, itemsLoading: false, items: action.items};
        case FETCH_ITEMS_FAILURE:
            return {...state, itemsLoading: false};
        case FETCH_ITEM_INFO_REQUEST:
            return {...state, itemLoading: true};
        case FETCH_ITEM_INFO_SUCCESS:
            return {...state, itemLoading: false, item: action.item};
        case FETCH_ITEM_INFO_FAILURE:
            return {...state, itemLoading: false};
        default:
            return state;
    }
};

export default itemsReducer;